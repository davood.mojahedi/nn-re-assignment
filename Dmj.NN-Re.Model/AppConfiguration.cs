﻿namespace Dmj.NN_Re.Model
{
    public class AppConfiguration
    {
        public string CurrentVendor { get; set; }

        public string VendorNumber1BaseUrl { get; set; }
        
        public string VendorNumber2BaseUrl { get; set; }
    }
}
