﻿namespace Dmj.NN_Re.Model
{
    public class Company 
    {
        public string Symbol { get; set; }
        
        public string Name { get; set; }
        
        public string Exchange { get; set; }

    }
}
