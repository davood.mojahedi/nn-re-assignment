﻿using System;

namespace Dmj.NN_Re.Model
{
    public class Quote
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public double LastPrice { get; set; }
        public double Change { get; set; }
        public double ChangePercent { get; set; }
        public DateTime Timestamp { get; set; }
        public double MSDate { get; set; }
        public long MarketCap { get; set; }
        public long Volume { get; set; }
        public double ChangeYTD { get; set; }
        public double ChangePercentYTD { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Open { get; set; }
    }
}
