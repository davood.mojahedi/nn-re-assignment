﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.Model;
using Microsoft.Extensions.Options;

namespace Dmj.NN_Re.ExternalService.Services
{
    internal class ExternalVendorNumber2QuoteInfoService : IExternalQuoteInfoService
    {
        private readonly AppConfiguration _appConfiguration;

        public ExternalVendorNumber2QuoteInfoService(IOptions<AppConfiguration> appConfig)
        {
            _appConfiguration = appConfig.Value;
        }

        public async Task<IEnumerable<Company>> GetAllCompaniesAsync()
        {
            var httpClient = new BaseHttpCaller<IEnumerable<Company>>
                (_appConfiguration.VendorNumber2BaseUrl);

            return await httpClient.GetServiceAsync("quoteInfo/companies");

        }

        public async Task<Quote> GetQuoteBySymbolAsync(string symbol)
        {

            var httpClient = new BaseHttpCaller<Quote>
                (_appConfiguration.VendorNumber2BaseUrl);

            var result = (await httpClient
                .GetServiceAsync($"quoteInfo/quoteBySymbol/{symbol}"));

            if (result==null)
                throw new InvalidDataException("Symbol is not exist");

            return result;
        }
    }
}
