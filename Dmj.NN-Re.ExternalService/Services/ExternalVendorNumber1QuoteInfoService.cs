﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.Model;
using Microsoft.Extensions.Options;

namespace Dmj.NN_Re.ExternalService.Services
{
    internal class ExternalVendorNumber1QuoteInfoService : IExternalQuoteInfoService
    {
        private readonly AppConfiguration _appConfiguration;

        public ExternalVendorNumber1QuoteInfoService(IOptions<AppConfiguration> appConfig)
        {
            _appConfiguration = appConfig.Value;
        }

        public async Task<IEnumerable<Company>> GetAllCompaniesAsync()
        {
            var httpClient = new BaseHttpCaller<Company>
                (_appConfiguration.VendorNumber1BaseUrl);

            var reader = await httpClient.GetFileServiceAsync("quoteInfo/ownCompanies");

            using var csvStreamReader = new StreamReader(reader);

            using var csvReader = new CsvReader(csvStreamReader, CultureInfo.CurrentCulture);

            return csvReader.GetRecords<Company>().ToList();

        }

        public async Task<Quote> GetQuoteBySymbolAsync(string symbol)
        {
            var httpClient = new BaseHttpCaller<Quote>
                (_appConfiguration.VendorNumber1BaseUrl);

            var reader = await httpClient.GetFileServiceAsync($"quoteInfo/currentQuoteBySymbol/{symbol}");

            using var csvStreamReader = new StreamReader(reader);

            using var csvReader = new CsvReader(csvStreamReader, CultureInfo.CurrentCulture);

            var readList = csvReader.GetRecords<Quote>().ToList();

            if (!readList.Any())
                throw new InvalidDataException("Symbol is not exist");

            return readList.FirstOrDefault();
        }
    }
}
