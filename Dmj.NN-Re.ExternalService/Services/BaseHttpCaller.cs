﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Dmj.NN_Re.ExternalService.Services
{
    public class BaseHttpCaller<T>
    {
        private readonly HttpClient _client;
        public BaseHttpCaller(string baseUrl)
        {
            BaseUrl = baseUrl;
            _client = new HttpClient();
        }

        public string BaseUrl { get; set; }

        public async Task<T>PostServiceAsync(string path, object body)
        {
            var response = await _client
                .PostAsJsonAsync($"{BaseUrl}/{path}", body);
            
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsAsync<T>();

            throw new Exception("Calling service is failed");
        }

        public async Task<T> GetServiceAsync(string path)
        {
            var response = await _client
                .GetAsync($"{BaseUrl}/{path}");

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsAsync<T>();

            throw new Exception("Calling service is failed");
        }

        public async Task<Stream> GetFileServiceAsync(string path)
        {
            var response = await _client
                .GetAsync($"{BaseUrl}/{path}");

            if (!response.IsSuccessStatusCode) 
                throw new Exception("Calling service is failed");

            return await response.Content.ReadAsStreamAsync();

        }
    }
}
