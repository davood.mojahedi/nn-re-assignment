﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CsvHelper.Configuration;
using Dmj.NN_Re.Common.Enum;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.Model;
using Microsoft.Extensions.Options;

namespace Dmj.NN_Re.ExternalService.Services
{
    public class ExternalQuoteInfoService :IExternalQuoteInfoService
    {
        private readonly IExternalQuoteInfoService _quoteInfoService;

        public ExternalQuoteInfoService(IOptions<AppConfiguration> appConfig)
        {
            var appConfiguration = appConfig.Value;

            if (appConfiguration.CurrentVendor == EVendors.VendorNumber1.ToString())
                _quoteInfoService = new ExternalVendorNumber1QuoteInfoService(appConfig);
            else if (appConfiguration.CurrentVendor == EVendors.VendorNumber2.ToString())
                _quoteInfoService = new ExternalVendorNumber2QuoteInfoService(appConfig);
            else
                throw new ConfigurationException("Current vendor is not valid");
        }

        public Task<IEnumerable<Company>> GetAllCompaniesAsync()
        {
            return _quoteInfoService.GetAllCompaniesAsync();
        }

        public Task<Quote> GetQuoteBySymbolAsync(string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
                throw new InvalidDataException("Symbol is invalid");

            return _quoteInfoService.GetQuoteBySymbolAsync(symbol);

        }
    }
}
