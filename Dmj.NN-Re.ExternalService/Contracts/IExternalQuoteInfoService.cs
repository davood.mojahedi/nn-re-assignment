﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dmj.NN_Re.Model;

namespace Dmj.NN_Re.ExternalService.Contracts
{
    public interface IExternalQuoteInfoService
    {
        Task<IEnumerable<Company>> GetAllCompaniesAsync();

        Task<Quote> GetQuoteBySymbolAsync(string symbol);

    }
}
