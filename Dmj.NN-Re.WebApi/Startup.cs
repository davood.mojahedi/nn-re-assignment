using Dmj.NN_Re.Common.Constants;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.ExternalService.Services;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;
using Dmj.NN_Re.Service.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Dmj.NN_Re.Assignment
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
      
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = SwaggerConstants.Title,
                    Version = SwaggerConstants.Version,
                    Description = SwaggerConstants.Description,
                    Contact = new OpenApiContact()
                    {
                        Name = SwaggerConstants.ContactName,
                        Email = SwaggerConstants.ContactEmail
                    }
                });
            });

            services.Configure<AppConfiguration>(Configuration.GetSection("AppConfiguration"));

            RegisterServices(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandler("/api/ExceptionHandler/Index");

            //it should be limited in real app for just allowed origin
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseRouting();

            app.UseSwagger();

            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint(SwaggerConstants.JsonPath, SwaggerConstants.Version);
                option.EnableFilter();
                option.DefaultModelExpandDepth(-1);
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

            });
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<IExternalQuoteInfoService, ExternalQuoteInfoService>();
            services.AddScoped<IQuoteService, QuoteService>();
            services.AddScoped<ICompanyService, CompanyService>();
        }
    }
}
