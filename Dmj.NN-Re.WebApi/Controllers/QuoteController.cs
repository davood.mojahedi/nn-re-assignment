﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Dmj.NN_Re.Assignment.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class QuoteController : ControllerBase
    {
        private readonly ICompanyService _companyService;
        private readonly IQuoteService _quoteService;

        public QuoteController(IQuoteService quoteService
            , ICompanyService companyService)
        {
            _quoteService = quoteService;
            _companyService = companyService;
        }

        [Route("company/list")]
        [HttpGet]
        public async Task<IEnumerable<Company>> CompanyListAsync()
        {
            return await _companyService.ListAsync();
        }

        [Route("info/{symbol}")]
        [HttpGet]
        public async Task<Quote> InfoAsync(string symbol)
        {
            return await _quoteService.GetQuoteBySymbolAsync(symbol);
        }
    }
}
