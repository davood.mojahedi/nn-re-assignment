﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dmj.NN_Re.Assignment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExceptionHandlerController : ControllerBase
    {
        private readonly ILogger<ExceptionHandlerController> _logger;

        public ExceptionHandlerController(ILogger<ExceptionHandlerController> logger)
        {
            _logger = logger;
        }

        [Route("Index")]
        [ApiExplorerSettings(IgnoreApi = true), HttpGet, HttpPost, HttpDelete, HttpPatch, HttpPut, HttpHead,
         HttpOptions, ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public string Index()
        {
            var message = CreateExceptionMessage(HttpContext);
            var errorIssuer = GetErrorIssuerExceptionMessage(HttpContext);

            _logger.LogCritical($"Exception >>> Message :{message} | errorIssuer {errorIssuer}");

            return message;
        }

        private static string CreateExceptionMessage(HttpContext context)
        {
            var exHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

            var error = exHandlerFeature?.Error;

            return GetDeepExceptionMessage(error);
        }

        private static string GetDeepExceptionMessage(Exception ex)
        {
            if (ex == null) return "";

            if (ex.InnerException != null)
            {
                return ex.InnerException.InnerException != null ? 
                    ex.InnerException.InnerException.Message : ex.InnerException.Message;
            }
            
            return ex.Message;

        }

        private static string GetErrorIssuerExceptionMessage(HttpContext context)
        {
            var exHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

            var urlError = GetUrlError(exHandlerFeature);

            return urlError;
        }


        private static string GetUrlError(IExceptionHandlerFeature src)
        {
            return src is null ?
                   string.Empty :
                   (string)src.GetType().GetProperty("Path")?.GetValue(src, null);
        }
    }
}
