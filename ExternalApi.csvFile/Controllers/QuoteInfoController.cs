﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using ExternalApi.Rest.Model;
using Microsoft.AspNetCore.Mvc;

namespace ExternalApi.csvFile.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuoteInfoController : ControllerBase
    {
        public QuoteInfoController()
        {
            FillFakeCompanyData();
            FillFakeQuoteData();
        }

        private IEnumerable<CompanyInfo> _allCompanyInfos;
        private IEnumerable<QuoteInfo> _allQuoteInfos;

        [Route("ownCompanies")]
        [HttpGet]
        public async Task<FileResult> GetCompanies()
        {
            var pathToRead = Path.Combine(Directory.GetCurrentDirectory(), $"companies_{DateTime.Now:HHmmss}.csv");
            await using (var writer = new StreamWriter(pathToRead))
            {
                await using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    await csv.WriteRecordsAsync(_allCompanyInfos);
                }
            }

            var memory = new MemoryStream();

            await using (var stream = new FileStream(pathToRead, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;
            return File(memory, "text/csv", Path.GetFileName(pathToRead));
        }

        [Route("currentQuoteBySymbol/{symbol}")]
        [HttpGet]
        public async Task<FileResult> GetQuoteBySymbol(string symbol)
        {
            var pathToRead = Path.Combine(Directory.GetCurrentDirectory(), $"quote_{DateTime.Now:HHmmss}.csv");
            var specQuote = _allQuoteInfos
                .FirstOrDefault(c => c.Symbol.ToLower() == symbol.ToLower());

            if (specQuote==null) return null;
            await using (var writer = new StreamWriter(pathToRead))
            {
                await using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    await csv.WriteRecordsAsync(new List<QuoteInfo>(){ specQuote });
                }
            }

            var memory = new MemoryStream();

            await using (var stream = new FileStream(pathToRead, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;
            return File(memory, "text/csv", Path.GetFileName(pathToRead));
        }

        private void FillFakeCompanyData()
        {
            _allCompanyInfos = new List<CompanyInfo>()
            {
                new CompanyInfo() {Symbol = "MSFT", Name = "Microsoft Corporation", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "NFLX", Name = "Netflix Inc", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "AAPL", Name = "Apple Inc", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "AMZN", Name = "Amazon.com, Inc", Exchange = "NASDAQ"}
            };
        }
        private void FillFakeQuoteData()
        {
            _allQuoteInfos = new List<QuoteInfo>()
            {
                new QuoteInfo()
                {
                    Symbol = "NFLX",
                    Name = "Netflix Inc",
                    LastPrice=524.49,
                    Change=15.6,
                    ChangePercent=3.06549549018453,
                    Timestamp= Convert.ToDateTime("2014/10/10 10:10:12"),
                    MSDate= 41570.568969907,
                    MarketCap=476497591530,
                    Volume=397562,
                    ChangeYTD=532.1729,
                    ChangePercentYTD=-1.44368493773359,
                    High=52499,
                    Low=519.175,
                    Open=519.175

                },
                new QuoteInfo()
                {
                    Symbol = "MSFT",
                    Name = "Microsoft Corporation",
                    LastPrice=319.15,
                    Change=11.1,
                    ChangePercent=2.8459549018453,
                    Timestamp= Convert.ToDateTime("2014/09/20 14:21:12"),
                    MSDate= 32548.1555555,
                    MarketCap=385497900530,
                    Volume=295145,
                    ChangeYTD=320.1252,
                    ChangePercentYTD=-2.5258443773359,
                    High=350,
                    Low=310.15,
                    Open=314.15

                },
                new QuoteInfo()
                {
                    Symbol = "AAPL",
                    Name = "Apple Inc",
                    LastPrice=195.15,
                    Change=15.21,
                    ChangePercent=1.00239549018453,
                    Timestamp= Convert.ToDateTime("2014/05/11 15:21:12"),
                    MSDate= 32644.1555555,
                    MarketCap=489537123530,
                    Volume=462145,
                    ChangeYTD=250.1255,
                    ChangePercentYTD=-1.3255443773359,
                    High=179,
                    Low=170.99,
                    Open=172.89
                },
                new QuoteInfo()
                {
                    Symbol = "AMZN",
                    Name = "Amazon.com, Inc",
                    LastPrice=3206.78,
                    Change=22.40,
                    ChangePercent=2.119945255,
                    Timestamp= Convert.ToDateTime("2014/06/05 19:21:12"),
                    MSDate= 42635.1022,
                    MarketCap=981457263530,
                    Volume=2283437,
                    ChangeYTD=470.1296,
                    ChangePercentYTD=-2.00003773359,
                    High=3396.78,
                    Low=3192.78,
                    Open=3276.78
                }
            };
        }
    }
}
