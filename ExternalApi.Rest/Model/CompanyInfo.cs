﻿namespace ExternalApi.Rest.Model
{
    public class CompanyInfo
    {
        public string Symbol { get; set; }

        public string Name { get; set; }

        public string Exchange { get; set; }
    }
}
