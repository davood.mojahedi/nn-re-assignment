﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExternalApi.Rest.Model;
using Microsoft.AspNetCore.Mvc;

namespace ExternalApi.Rest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuoteInfoController : ControllerBase
    {
        public QuoteInfoController()
        {
            FillFakeCompanyData();
            FillFakeQuoteData();
        }

        private IEnumerable<CompanyInfo> _allCompanyInfos;
        private IEnumerable<QuoteInfo> _allQuoteInfos;

        [Route("companies")]
        [HttpGet]
        public IEnumerable<CompanyInfo> GetCompanies()
        {
            return _allCompanyInfos;
        }

        [Route("quoteBySymbol/{symbol}")]
        [HttpGet]
        public QuoteInfo GetQuoteBySymbol(string symbol)
        {
            return _allQuoteInfos
                .FirstOrDefault(c=>c.Symbol.ToLower()==symbol.ToLower());
        }

        private void FillFakeCompanyData()
        {
            _allCompanyInfos = new List<CompanyInfo>()
            {
                new CompanyInfo() {Symbol = "MSFT", Name = "Microsoft Corporation", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "NFLX", Name = "Netflix Inc", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "AAPL", Name = "Apple Inc", Exchange = "NASDAQ"},
                new CompanyInfo() {Symbol = "AMZN", Name = "Amazon.com, Inc", Exchange = "NASDAQ"}
            };
        }
        private void FillFakeQuoteData()
        {
            _allQuoteInfos = new List<QuoteInfo>()
            {
                new QuoteInfo()
                {
                    Symbol = "NFLX",
                    Name = "Netflix Inc",
                    LastPrice=524.49,
                    Change=15.6,
                    ChangePercent=3.06549549018453,
                    Timestamp= Convert.ToDateTime("2014/10/10 10:10:12"),
                    MSDate= 41570.568969907,
                    MarketCap=476497591530,
                    Volume=397562,
                    ChangeYTD=532.1729,
                    ChangePercentYTD=-1.44368493773359,
                    High=52499,
                    Low=519.175,
                    Open=519.175

                },
                new QuoteInfo()
                {
                    Symbol = "MSFT",
                    Name = "Microsoft Corporation",
                    LastPrice=319.15,
                    Change=11.1,
                    ChangePercent=2.8459549018453,
                    Timestamp= Convert.ToDateTime("2014/09/20 14:21:12"),
                    MSDate= 32548.1555555,
                    MarketCap=385497900530,
                    Volume=295145,
                    ChangeYTD=320.1252,
                    ChangePercentYTD=-2.5258443773359,
                    High=350,
                    Low=310.15,
                    Open=314.15

                },
                new QuoteInfo()
                {
                    Symbol = "AAPL",
                    Name = "Apple Inc",
                    LastPrice=195.15,
                    Change=15.21,
                    ChangePercent=1.00239549018453,
                    Timestamp= Convert.ToDateTime("2014/05/11 15:21:12"),
                    MSDate= 32644.1555555,
                    MarketCap=489537123530,
                    Volume=462145,
                    ChangeYTD=250.1255,
                    ChangePercentYTD=-1.3255443773359,
                    High=179,
                    Low=170.99,
                    Open=172.89
                },
                new QuoteInfo()
                {
                    Symbol = "AMZN",
                    Name = "Amazon.com, Inc",
                    LastPrice=3206.78,
                    Change=22.40,
                    ChangePercent=2.119945255,
                    Timestamp= Convert.ToDateTime("2014/06/05 19:21:12"),
                    MSDate= 42635.1022,
                    MarketCap=981457263530,
                    Volume=2283437,
                    ChangeYTD=470.1296,
                    ChangePercentYTD=-2.00003773359,
                    High=3396.78,
                    Low=3192.78,
                    Open=3276.78
                }
            };
        }
    }
}
