﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dmj.NN_Re.Model;

namespace Dmj.NN_Re.Service.Contracts
{
    public interface ICompanyService
    {
        Task<IEnumerable<Company>> ListAsync();
    }
}
