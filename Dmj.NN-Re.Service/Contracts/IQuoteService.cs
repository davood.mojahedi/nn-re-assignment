﻿using System.Threading.Tasks;
using Dmj.NN_Re.Model;

namespace Dmj.NN_Re.Service.Contracts
{
    public interface IQuoteService
    {
        Task<Quote> GetQuoteBySymbolAsync(string symbol);
    }
}
