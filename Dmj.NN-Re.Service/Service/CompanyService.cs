﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;

namespace Dmj.NN_Re.Service.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly IExternalQuoteInfoService _externalQuoteInfoService;

        public CompanyService(IExternalQuoteInfoService externalQuoteInfoService)
        {
            _externalQuoteInfoService = externalQuoteInfoService;
        }

        public async Task<IEnumerable<Company>> ListAsync()
        {
            return await _externalQuoteInfoService.GetAllCompaniesAsync();
        }
    }
}
