﻿using System.Threading.Tasks;
using Dmj.NN_Re.ExternalService.Contracts;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;

namespace Dmj.NN_Re.Service.Service
{
    public class QuoteService : IQuoteService
    {
        private readonly IExternalQuoteInfoService _externalQuoteInfoService;

        public QuoteService(IExternalQuoteInfoService externalQuoteInfoService)
        {
            _externalQuoteInfoService = externalQuoteInfoService;
        }

        public async Task<Quote> GetQuoteBySymbolAsync(string symbol)
        {
            return await _externalQuoteInfoService.GetQuoteBySymbolAsync(symbol);
        }
    }
}
