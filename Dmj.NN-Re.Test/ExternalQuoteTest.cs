using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper.Configuration;
using Dmj.NN_Re.Assignment.Controllers;
using Dmj.NN_Re.ExternalService.Services;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace Dmj.NN_Re.Test
{
    public class ExternalQuoteTest
    {
        private IOptions<AppConfiguration> _config;
        private Mock<ICompanyService> _companyMoq;
        private Mock<IQuoteService> _quoteMoq;
        
        [SetUp]
        public void Setup()
        {
            _companyMoq = new Mock<ICompanyService>();
            _quoteMoq = new Mock<IQuoteService>();
            var companyData = new List<Company>()
            {
                new Company() {Symbol = "AAPL", Name = "Apple Inc", Exchange = "NASDAQ"},
                new Company() {Symbol = "MSFT", Name = "Microsoft Corporation", Exchange = "NASDAQ"},
                new Company() {Symbol = "NFLX", Name = "Netflix Inc", Exchange = "NASDAQ"}
            };
            var appleQuote = new Quote()
            {
                Symbol = "AAPL",
                Name = "Apple Inc",
                LastPrice = 195.15,
                Change = 15.21,
                ChangePercent = 1.00239549018453,
                Timestamp = Convert.ToDateTime("2014/05/11 15:21:12"),
                MSDate = 32644.1555555,
                MarketCap = 489537123530,
                Volume = 462145,
                ChangeYTD = 250.1255,
                ChangePercentYTD = -1.3255443773359,
                High = 179,
                Low = 170.99,
                Open = 172.89
            };
            _companyMoq.Setup(s => s.ListAsync())
                .ReturnsAsync(companyData);

            _quoteMoq.Setup(s => s.GetQuoteBySymbolAsync("AAPL"))
                .ReturnsAsync(appleQuote);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();

            _config = Options.Create(configuration.GetSection("AppConfiguration").Get<AppConfiguration>());
        }
        
        [Test]
        public void Should_Rise_Exception_if_Symbol_is_empty()
        {
            //Arrange
            var externalService = new ExternalQuoteInfoService(_config);
            
            //Assert
            Assert.Throws<InvalidDataException>(() => externalService.GetQuoteBySymbolAsync(""));
        }

        [Test]
        public void Should_Rise_Exception_if_Symbol_is_Not_Exist()
        {
            //Arrange
            var nullQuoteMoq = new Mock<IQuoteService>();
            nullQuoteMoq.Setup(s => s.GetQuoteBySymbolAsync("wrongSym"))
                .Throws<InvalidDataException>();

            var testController = new QuoteController(nullQuoteMoq.Object, _companyMoq.Object);

            //Assert
            Assert.ThrowsAsync<InvalidDataException>(async () => await testController.InfoAsync("wrongSym"));
        }



        [Test]
        public void Should_Rise_Exception_if_config_invalid()
        {
            _config.Value.CurrentVendor = "wrongVendor";
            //Assert
            Assert.Throws<ConfigurationException>(() => new ExternalQuoteInfoService(_config));
        }
    }
}