using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dmj.NN_Re.Assignment.Controllers;
using Dmj.NN_Re.Model;
using Dmj.NN_Re.Service.Contracts;
using Moq;
using NUnit.Framework;

namespace Dmj.NN_Re.Test
{
    public class CompanyTest
    {
        private Mock<ICompanyService> _companyMoq;
        private Mock<IQuoteService> _quoteMoq;
        [SetUp]
        public void Setup()
        {
            _companyMoq = new Mock<ICompanyService>();
            _quoteMoq = new Mock<IQuoteService>();
            var companyData = new List<Company>()
            {
                new Company() {Symbol = "AAPL", Name = "Apple Inc", Exchange = "NASDAQ"},
                new Company() {Symbol = "MSFT", Name = "Microsoft Corporation", Exchange = "NASDAQ"},
                new Company() {Symbol = "NFLX", Name = "Netflix Inc", Exchange = "NASDAQ"}
            };
            var appleQuote = new Quote()
            {
                Symbol = "AAPL",
                Name = "Apple Inc",
                LastPrice = 195.15,
                Change = 15.21,
                ChangePercent = 1.00239549018453,
                Timestamp = Convert.ToDateTime("2014/05/11 15:21:12"),
                MSDate = 32644.1555555,
                MarketCap = 489537123530,
                Volume = 462145,
                ChangeYTD = 250.1255,
                ChangePercentYTD = -1.3255443773359,
                High = 179,
                Low = 170.99,
                Open = 172.89
            };
            _companyMoq.Setup(s => s.ListAsync())
                .ReturnsAsync(companyData);

            _quoteMoq.Setup(s => s.GetQuoteBySymbolAsync("AAPL"))
                .ReturnsAsync(appleQuote);
        }

        [Test]
        public async Task Should_Mock_Data_Get_List()
        {
            //Arrange
            
            //Act
            var testController = new QuoteController(_quoteMoq.Object, _companyMoq.Object);
            var readData = (await testController.CompanyListAsync()).ToList();
            //Assert
            Assert.AreEqual(readData.Count(), 3);
            Assert.IsTrue(readData.Last().Symbol == "NFLX");
        }
    }
}