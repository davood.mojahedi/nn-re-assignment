﻿namespace Dmj.NN_Re.Common.Constants
{
    public class SwaggerConstants
    {
        public const string Title = "NN Re Group Assignment ";

        public const string Version = "V1.0";

        public const string JsonPath = "/swagger/v1/swagger.json";

        public const string Description = "Swagger for NN Re Group Assignment controllers";

        public const string ContactName = "Davood Mojahedi";

        public const string ContactEmail = "davood.mojahedi@gmail.com";
    }
}
